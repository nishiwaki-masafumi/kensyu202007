<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>第三回課題、ログイン画面練習2 西脇</title>
  </head>
  <body>
    <h1>第三回課題、ログイン画面練習2 西脇</h1>

      <form method='POST' action='result02.php'>
        <!--
          ここにform部品を自由に配置してみよう
          送信ボタンとリセットボタンも忘れずに
        --->
        ログインidとパスワードを入力してください。
        <br/>

        <table border='1' cellpadding='1' cellspacing='1'>
            <tr>
                <td>id</td>
                <td><input type='text' name='id' required="required"></td>
            </tr>
            <tr>
                <td>パスワード</td>
                <td><input type='password' name='password' required="required"></td>
            </tr>
        </table>
        <br/>
        <input type='submit' value=' ログイン '>
      </form>
  </body>
</html>
