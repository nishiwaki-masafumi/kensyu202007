<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>第三回課題、指定行数でテーブル出力 西脇</title>
  </head>
  <body>
    <h1>第三回課題、指定行数でテーブル出力 西脇</h1>
    <form method='GET' action='loop01.php'>
      <!--
        ここにform部品を自由に配置してみよう
        送信ボタンとリセットボタンも忘れずに
      --->
        <input type='text' name='cols'>行のテーブルを生成する<br/>
        <input type='submit' value=' 送信 '>
        <input type='submit' value=' リセット '><br/>
    </form>
    <hr>
    <table table border='1'>
    <?php
      for($i=0; $i < $_GET['cols']; $i++){
        echo
            '<tr>
                <td>一郎</td>
                <td>二郎</td>
                <td>三郎</td>
                <td>四郎</td>
                <td>五郎</td>
            </tr>';
      }
    ?>
    </table>
  </body>
</html>
