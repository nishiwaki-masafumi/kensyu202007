<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>第三回課題、行数・列数を指定してテーブル出力 西脇</title>
  </head>
  <body>
    <h1>第三回課題、行数・列数を指定してテーブル出力 西脇</h1>
    <form method='GET' action='loop02.php'>
      <!--
        ここにform部品を自由に配置してみよう
        送信ボタンとリセットボタンも忘れずに
      --->
        <input type='text' name='raw'>行 x
        <input type='text' name='column'>列<br/>
        <input type='submit' value=' 送信 '>
        <input type='submit' value=' リセット '><br/>
    </form>
    <hr>
    <table table border='1'>
    <?php
        for($i=0; $i < $_GET['raw']; $i++){
            echo '<tr>';
                for($n=0; $n < $_GET['column']; $n++){
                    echo '<td>一郎</td>';
                }
            echo '</tr>';
        }
    ?>
    </table>
    <br/>

    HTMLでTABLEタグを出してみて、構造を理解
    <table table border='1'>
        <tr>
            <td>1-1</td>
            <td>1-2</td>
        </tr>
        <tr>
            <td>2-2</td>
            <td>2-3</td>
        </tr>
    </table>
  </body>
</html>
