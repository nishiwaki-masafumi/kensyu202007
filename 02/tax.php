<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>第二回課題、消費税計算ページ 西脇</title>
  </head>
  <body>
    <h1>第二回課題、消費税計算ページ 西脇</h1>

    <form method='POST' action='tax.php'>
      <!--
        ここにform部品を自由に配置してみよう
        送信ボタンとリセットボタンも忘れずに
      --->
      <table border='1' cellpadding='1' cellspacing='1'>
          <th>商品名</th>
          <th>価格(単位:円、税抜)</th>
          <th>個数</th>
          <th>税率</th>
          <tr>
              <td><input type='text' name='namae1' size="15"></td>
              <td><input type='text' name='kakaku1' size="15">円</td>
              <td><input type='text' name='kosuu1' size="5">個</td>
              <td>
                  <input type='radio' name='zeiritsu1' value='8' checked>8%
                  <input type='radio' name='zeiritsu1' value='10'>10%
              </td>
          </tr>
          <tr>
              <td><input type='text' name='namae2' size="15"></td>
              <td><input type='text' name='kakaku2' size="15">円</td>
              <td><input type='text' name='kosuu2' size="5">個</td>
              <td>
                  <input type='radio' name='zeiritsu2' value='8' checked>8%
                  <input type='radio' name='zeiritsu2' value='10'>10%
              </td>
          </tr>
          <tr>
              <td><input type='text' name='namae3' size="15"></td>
              <td><input type='text' name='kakaku3' size="15">円</td>
              <td><input type='text' name='kosuu3' size="5">個</td>
              <td>
                  <input type='radio' name='zeiritsu3' value='8' checked>8%
                  <input type='radio' name='zeiritsu3' value='10'>10%
              </td>
          </tr>
          <tr>
              <td><input type='text' name='namae4' size="15"></td>
              <td><input type='text' name='kakaku4' size="15">円</td>
              <td><input type='text' name='kosuu4' size="5">個</td>
              <td>
                  <input type='radio' name='zeiritsu4' value='8' checked>8%
                  <input type='radio' name='zeiritsu4' value='10'>10%
              </td>
          </tr>
          <tr>
              <td><input type='text' name='namae5' size="15"></td>
              <td><input type='text' name='kakaku5' size="15">円</td>
              <td><input type='text' name='kosuu5' size="5">個</td>
              <td>
                  <input type='radio' name='zeiritsu5' value='8' checked>8%
                  <input type='radio' name='zeiritsu5' value='10'>10%
              </td>
          </tr>
      </table>

      <input type='submit' value=' 送信 '>
      <input type='reset' value=' リセット '>

    </form>

    <br>
    <hr>
    <br>

    <table border='1' cellpadding='1' cellspacing='1'>
        <th>商品名</th>
        <th>価格(単位:円、税抜)</th>
        <th>個数</th>
        <th>税率</th>
        <th>小計</th>
        <tr>
            <td><?php echo $_POST['namae1']; ?></td>
            <td><?php echo $_POST['kakaku1'];?>円</td>
            <td><?php echo $_POST['kosuu1']; ?>個</td>
            <td><?php echo $_POST['zeiritsu1']; ?>%</td>
            <td>
                <?php
                    $tanka1 = $_POST['kakaku1'];
                    $number1 = $_POST['kosuu1'];
                    $tax1 = $_POST['zeiritsu1'];
                    $total1 = $tanka1 * $number1 * (($tax1 * 0.01) + 1);
                    echo number_format($total1) . '円';

                    /*
                    最初に発想したコード
                    formから持ってきた値が8%or10%と等しいか判定して税率を計算する処理を入れたが、
                    ラジオボタンで入力される税率は8と10しかないので微妙な感じになりました。

                    if($tax == '8'){
                        $total = $total * 1.08;
                        echo $total . '円（税込み）';
                    } elseif($tax == '10'){
                        $total = $total * 1.10;
                        echo $total . '円（税込み）';
                    }
                    */
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo $_POST['namae2']; ?></td>
            <td><?php echo $_POST['kakaku2']; ?>円</td>
            <td><?php echo $_POST['kosuu2']; ?>個</td>
            <td><?php echo $_POST['zeiritsu2']; ?>%</td>
            <td>
                <?php
                    $tanka2 = $_POST['kakaku2'];
                    $number2 = $_POST['kosuu2'];
                    $tax2 = $_POST['zeiritsu2'];
                    $total2 = $tanka2 * $number2 * (($tax2 * 0.01) + 1);
                    echo number_format($total2) . '円';
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo $_POST['namae3']; ?></td>
            <td><?php echo $_POST['kakaku3']; ?>円</td>
            <td><?php echo $_POST['kosuu3']; ?>個</td>
            <td><?php echo $_POST['zeiritsu3']; ?>%</td>
            <td>
                <?php
                    $tanka3 = $_POST['kakaku3'];
                    $number3 = $_POST['kosuu3'];
                    $tax3 = $_POST['zeiritsu3'];
                    $total3 = $tanka3 * $number3 * (($tax3 * 0.01) + 1);
                    echo number_format($total3) . '円';
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo $_POST['namae4']; ?></td>
            <td><?php echo $_POST['kakaku4']; ?>円</td>
            <td><?php echo $_POST['kosuu4']; ?>個</td>
            <td><?php echo $_POST['zeiritsu4']; ?>%</td>
            <td>
                <?php
                    $tanka4 = $_POST['kakaku4'];
                    $number4 = $_POST['kosuu4'];
                    $tax4 = $_POST['zeiritsu4'];
                    $total4 = $tanka4 * $number4 * (($tax4 * 0.01) + 1);
                    echo number_format($total4) . '円';
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo $_POST['namae5']; ?></td>
            <td><?php echo $_POST['kakaku5']; ?>円</td>
            <td><?php echo $_POST['kosuu5']; ?>個</td>
            <td><?php echo $_POST['zeiritsu5']; ?>%</td>
            <td>
                <?php
                    $tanka5 = $_POST['kakaku5'];
                    $number5 = $_POST['kosuu5'];
                    $tax5 = $_POST['zeiritsu5'];
                    $total5 = $tanka5 * $number5 * (($tax5 * 0.01) + 1);
                    echo number_format($total5) . '円';
                ?>
            </td>
        </tr>
        <tr></tr>
            <td colspan='4'>合計</td>
            <td>
                <?php
                    $all_total = $total1  + $total2 + $total3 + $total4 + $total5;
                    echo number_format($all_total) . '円';
                ?>
            </td>
    </table>

    <pre>
        <?php var_dump($_POST); ?>
    </pre>

  </body>
</html>
