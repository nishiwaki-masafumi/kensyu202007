<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>第二回課題、php練習 西脇</title>
  </head>
  <body>
    <h1>第二回課題、php練習 西脇</h1>
    <pre><?php var_dump($_GET); ?></pre>

    ようこそ「
    <?php echo $_GET['namae'] ?>
    」さん

    <br/>
    <br/>
    <table border='1' cellpadding='1' cellspacing='1'>
        <tr>
            <td>出身</td>
            <td><?php echo $_GET['shusshin']; ?></td>
        </tr>
        <tr>
            <td>性別</td>
            <td><?php echo $_GET['seibetsu']; ?></td>
        </tr>
        <tr>
            <td>好きな食べ物</td>
            <td>
                <?php
                    if (isset($_GET['tabemono'])) {
                        $tab = implode(', ' , $_GET['tabemono']);
                        echo $tab . '<br/>';
                    } else {
                        echo 'チェックされていません<br/>';
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td>電話番号</td>
            <td><?php echo $_GET['tel']; ?></td>
        </tr>
        <tr>
            <td>メッセージ</td>
            <td><?php echo $_GET['msg']; ?></td>
        </tr>
    </table>
    <br/>
    php内で改行処理の練習
    <?php
        echo "<br/>";
        echo $_GET['msg'];
        echo "<br/>";
        echo $_GET['msg'];
    ?>
    <?php
        $namae='おだて';
        $yoisyo='ビール腹';
    ?>
  </body>
</html>
