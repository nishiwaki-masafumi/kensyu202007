<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>第五回課題 連想配列、配列のネスト5 西脇</title>
  </head>
  <body>
    <h1>第五回課題 連想配列、配列のネスト5 西脇</h1>
    <h2>二次元配列 データ構造の作成（自由なデータ作成）</h2>
    <h3>大昔に運転したことのあるバイク</h3>

    <table table border='1'>
    <?php
        //配列の定義
        $bikes = array(
            array(
                'bike' => 'ZZR400',
                'maker' => 'カワサキ',
                'type' => 'ツアラー',
                'cc' => '400',
                'year' => '1990',
            ),
            array(
                'bike' => 'アドレスv125',
                'maker' => 'スズキ',
                'type' => 'スクーター',
                'cc' => '125',
                'year' => '2005',
            ),
            array(
                'bike' => 'マジェスティ',
                'maker' => 'ヤマハ',
                'type' => 'ビッグスクーター',
                'cc' => '250',
                'year' => '1995',
            ),
            array(
                'bike' => 'CB400',
                'maker' => 'ホンダ',
                'type' => 'ネイキッド',
                'cc' => '400',
                'year' => '1992',
            ),
        );

        //ヘッダーをテーブルで表示（不正解かも…）
        echo '<tr>';
        echo '<th>' . '車種' . '</th>'
           . '<th>' . 'メーカー' . '</th>'
           . '<th>' . 'タイプ' . '</th>'
           . '<th>' . '排気量' . '</th>'
           . '<th>' . '発売年' . '</th>'
           . '</tr>';
        //二次元連想配列の値をテーブルで表示
        foreach($bikes as $raw){
            echo '<tr>';
            foreach($raw as $value){
                echo '<td>' . $value . '</td>';
            }
            echo '</tr>';
        }
    ?>
    </table>
    <br/>
    <h2>var_dumpで配列の内容を出力</h2>
    <pre>
    <?php
        var_dump($bikes);
    ?>
    </pre>
  </body>
</html>
