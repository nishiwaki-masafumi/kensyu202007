<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>第五回課題 連想配列、配列のネスト4 西脇</title>
  </head>
  <body>
    <h1>第五回課題 連想配列、配列のネスト4 西脇</h1>
    <h2>二次元配列 データ構造の作成</h2>
    <table table border='1'>
    <?php
        //配列の定義
        $players = array(
            array(
                'id' => '3',
                'name' => '梶谷隆幸',
                'position' => '外野手',
                'from' => '島根',
                'year' => '2007',
            ),
            array(
                'id' => '4',
                'name' => '佐野恵太',
                'position' => '外野手',
                'from' => '島根',
                'year' => '2017',
            ),
            array(
                'id' => '15',
                'name' => '井納翔一',
                'position' => '投手',
                'from' => '東京',
                'year' => '2013',
            ),
        );

        /*単純なデータ構造の表示例
        foreach($players as $value){
            echo '背番号' . $value['id'] . ','
               . '名前' . $value['name'] . ','
               .'ポジション' . $value['position'] . ','
               .'出身地' . $value['from'] . ','
               .'入団年' . $value['year'] . '<br/>';
        }
        */


        //ヘッダーをテーブルで表示（不正解かも…）
        echo '<tr>';
        echo '<th>' . '背番号' . '</th>'
           . '<th>' . '名前' . '</th>'
           . '<th>' . 'ポジション' . '</th>'
           . '<th>' . '出身地' . '</th>'
           . '<th>' . '入団年' . '</th>'
           . '</tr>';
        //二次元連想配列の値をテーブルで表示
        foreach($players as $raw){
            echo '<tr>';
            foreach($raw as $value){
                echo '<td>' . $value . '</td>';
            }
            echo '</tr>';
        }
    ?>
    </table>
    <br/>
    <h2>var_dumpで配列の内容を出力</h2>
    <pre>
    <?php
        var_dump($players);
    ?>
    </pre>
  </body>
</html>
