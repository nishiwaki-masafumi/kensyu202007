<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>第五回課題 連想配列、配列のネスト2 西脇</title>
  </head>
  <body>
    <h1>第五回課題 連想配列、配列のネスト2 西脇</h1>
    <h2>すべての要素を出力する（foreach())</h2>
    <table table border='1'>
    <?php
        //1つ目のテーブル（書き方その1）
        $me_data1 = array(
            'fruit1' => 'スイカ',
            'sport1' => '野球',
            'town1' => '横浜',
            'age1' => '21',
            'food1' => 'カレーライス'
        );

        foreach($me_data1 as $each){
            echo '<tr>';
            echo '<td>' . $each . '</td>';
            echo '</tr>';
        }
    ?>
    </table>

    <br/>

    <table table border='1'>
    <?php
        //2つ目のテーブル（書き方その2）
        //$keyは連想配列のkey（添字、インデックス）という意味を思い出す
        //$key => $valueで添字を処理する意味が分かっていない
        foreach($me_data1 as $key => $value){
            echo '<tr>';
            echo '<td>' . $key . '</td>';
            echo '<td>' . $value . '</td>';
            echo '</tr>';
        }
    ?>
    </table>
    <br/>
    <h2>var_dumpで配列の内容を出力</h2>
    <pre>
    <?php
        var_dump($me_data1);
    ?>
    </pre>
  </body>
</html>
