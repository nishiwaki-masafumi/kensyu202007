<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>第五回課題 連想配列、配列のネスト1 西脇</title>
  </head>
  <body>
    <h1>第五回課題 連想配列、配列のネスト1 西脇</h1>
    <h2>連想配列の基本的な書きかた・表示のさせ方</h2>

    <?php
        //書き方その1
        $me_data1 = array(
            'fruit1' => 'スイカ',
            'sport1' => '野球',
            'town1' => '横浜',
            'age1' => '21',
            'food1' => 'カレーライス'
        );
        echo '書き方その1：' . $me_data1['town1'] . '<br/>'; //横浜と表示される

        //書き方その2（php5.4以降）
        $me_data2 = [
            'fruit2' => 'ブドウ',
            'sport2' => 'サッカー',
            'town2' => 'さいたま',
            'age2' => '42',
            'food2' => 'スパゲッティ'
        ];
        echo '書き方その2：' . $me_data2['town2'] . '<br/>'; //さいたまと表示される

        //書き方その3
        $me_data3['fruit3'] = 'ミカン';
        $me_data3['sport3'] = 'バスケ';
        $me_data3['town3'] = '千葉';
        $me_data3['age3'] = '63';
        $me_data3['food3'] = '寿司';
        echo '書き方その3：' . $me_data3['town3']; //千葉と表示される
    ?>
    <br/>
    <h2>var_dumpで配列の内容を出力</h2>
    <pre>
    <?php
        var_dump($me_data1);
        var_dump($me_data2);
        var_dump($me_data3);
    ?>
    </pre>
  </body>
</html>
