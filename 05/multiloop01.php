<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>第五回課題 連想配列、配列のネスト3 西脇</title>
  </head>
  <body>
    <h1>第五回課題 連想配列、配列のネスト3 西脇</h1>
    <h2>二次元配列</h2>
    <table table border='1'>
    <?php
        //配列の定義
        $place_a = array('千歳','札幌','函館','小樽','旭川');
        $place_b = array('川口','さいたま','所沢','川越','秩父');
        $place_c = array('横浜','川崎','藤沢','相模原','鎌倉');
        $place_d = array('新宿','豊島','中野','杉並','練馬');
        $place_e = array('福岡','北九州','久留米','太宰府','筑紫野');

        $place_all = array($place_a, $place_b, $place_c , $place_d , $place_e);

        foreach($place_all as $raw){
            echo '<tr>';
            foreach($raw as $value){
                echo '<td>' . $value . '</td>';
            }
            echo '</tr>';
        }

    ?>
    </table>

    <br/>
    <br/>
    <h2>var_dumpで配列の内容を出力</h2>
    <pre>
    <?php
        var_dump($place_all);
    ?>
    </pre>
  </body>
</html>
