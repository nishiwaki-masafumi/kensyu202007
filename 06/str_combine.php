<!DOCTYPE html>
<?php
    $str_a = 'サントリー';
    $str_b = 'のんある';
    $str_c = '気分';

    $str_name = 'レモンサワー';
    $str_aji = 'おいしい！';
?>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width,initial-scale=1'>
        <title>文字列結合補講 西脇</title>
    </head>
    <body>
        <h1>文字列結合補講 西脇</h1>
        <?php
            // １つめ
            echo $str_a . $str_b . $str_c . '<br/>';

            // ２つめ
            $str_d = $str_a . $str_b . $str_c;
            echo $str_d . '<br/>';

            // ３つめ
            $str_d = null;
            $str_d = $str_d . $str_a;
            $str_d = $str_d . $str_b;
            $str_d = $str_d . $str_c;
            echo $str_d . '<br/>';

            // ４つめ
            $str_d = null;
            $str_d .= $str_a;
            $str_d .= $str_b;
            $str_d .= $str_c;
            echo $str_d . '<br/><br/>';

            // ５つめ
            echo 'こんにちは！' . $str_aji . $str_name . 'さん！' . '<br/>';

            // ６つめ
            echo $str_name . '課長！今日も' . $str_aji . 'ですね！'
                           . $str_name . 'さん！' . '<br/>';

            // ７つめ
            $str_temp = $str_name;
            $str_temp .= 'さん今日も';
            $str_temp .= $str_aji;
            $str_temp .= 'ですね！';
            echo $str_temp . '<br/>';

            // ８つめ '='で上書き
            $str_aji = 'まろやか';
            echo $str_name . '課長！今日も' . $str_aji . 'ですね！'
                           . $str_name . 'さん！';
        ?>
    </body>
</html>
