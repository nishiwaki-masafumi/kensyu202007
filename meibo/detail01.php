<!DOCTYPE html>
<!-- 社員情報詳細(#03) -->

<!-- CSS呼び出し -->
<link rel="stylesheet" type="text/css" href="./include/style.css">

<pre>
<?php
    // $_GETの中身（URLのGETパラメータ）を出力する
    // var_dump($_GET);
?>
</pre>

<?php
    // common
    // static.phpを取り込む
    include('./include/statics.php');
    $DB_DSN = 'mysql:host=localhost; dbname=mnishiwaki; charset=utf8';
    $DB_USER = 'webaccess';
    $DB_PW = 'toMeu4rH';
    $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
    // $pdo = initDB();

    // $param_IDを定義
    // $_GETが存在するかつ中身が空でない場合、$_GETの中身を$param_IDに格納
    $param_ID = "";
    $error = "0";
    // if(empty($_GET['member_ID'])){
    if(isset($_GET['member_ID']) && $_GET['member_ID'] != ""){
        $param_ID = $_GET['member_ID'];
    }else{
        $error = "1";  // エラーフラグ設定
    }
?>

<?php
    // URLの$_GETパラメータが$paramに格納されているか確認
    // echo "param_ID = " . $param_ID . "<br/><br/>";

    // WHERE条件に指定しているメンバーIDのパラメータは$_GETの値を$param_IDに格納したもの
    $query_str = "SELECT mb.member_ID,mb.name,mb.pref,mb.seibetu,mb.age,sm.section_name,gm.grade_name
                  FROM member AS mb
                  LEFT JOIN section1_master AS sm
                  ON mb.section_ID = sm.ID
                  LEFT JOIN grade_master AS gm
                  ON mb.grade_ID = gm.ID
                  WHERE member_ID = " . $param_ID;

    // SQLの検索結果を$resultに格納
    // echo $query_str . '<br/><br/>';
    $sql = $pdo->prepare($query_str);
    $sql->execute();
    $result = $sql->fetchAll();


    $result_cnt = count($result);
    if($result_cnt != "1"){
        $error = "1";  // エラーフラグ設定
    }
?>

<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width,initial-scale=1'>
        <link rel='stylesheet' href='./include/style.css'>
        <title>社員名簿システム</title>
    </head>

    <!-- JS処理 -->
    <script type="text/javascript">
        // 削除ダイアログ処理
        function delData(){
            if(window.confirm('削除を行います。よろしいですか？')){
                //documentはHTML全体。sakujyoフォームをsubmitする
                document.sakujyo.submit();
            }
        }
    </script>

    <body>
        <!-- ヘッダーHTMLの取り込み -->
        <?php include("./include/header.php"); ?>

        <!--<pre>
        <?php
            // var_dumpで二次元連想配列の内容を確認
            // var_dump($result);
        ?>
        </pre>-->
        <br/>
        <div class='detail-result'>
        <table table border='1' width='100%' class='detail-result-table'>
        <?php
            // エラーフラグ判定
            if($error == "0"){

                // $resultは外側の配列になる。$eachに内側の配列の値を格納する
                // $eachに$result[0]の内容を格納しない場合は
                // $result[0]['member_ID']でも出力することができる
                $each = $result[0];
        ?>

                <tr>
                    <th>社員ID</hd>
                    <td><?php echo $each['member_ID'] ?></td>
                </tr>
                <tr>
                    <th>名前</hd>
                    <td><?php echo $each['name'] ?></td>
                </tr>
                <tr>
                    <th>出身地</th>
                    <td><?php echo $pref_array[$each['pref']] ?></td>
                </tr>
                <tr>
                    <th>性別</th>
                    <td><?php echo $gender_array[$each['seibetu']] ?></td>
                </tr>
                <tr>
                    <th>年齢</th>
                    <td><?php echo $each['age'] ?>才</td>
                </tr>
                <tr>
                    <th>所属部署</th>
                    <td><?php echo $each['section_name'] ?></td>
                </tr>
                <tr>
                    <th>役職</th>
                    <td><?php echo $each['grade_name'] ?></td>
                </tr>
        </table>

        <br/>

        <table>
        <form method='POST' action='./entry_update01.php'>
            <!-- 編集ボタン選択で社員データ更新ページにmember_IDを渡すための処理 -->
            <input type='hidden' name='member_ID' value='<?php echo $each['member_ID'] ?>'>
            <tr>
                <td><input type='submit' value='編集'></td>
        </form>

        <!-- 削除ボタン選択で社員データ削除ページにmember_IDを渡すための処理 -->
        <!-- formタグにJS処理をするためのname「sakujyo」を定義 -->
        <form method='POST' name='sakujyo' action='./delete01.php'>

                <td><input type='hidden' name='member_ID' value='<?php echo $each['member_ID'] ?>'></td>
                <!-- JSの処理を呼び出すためにonClick='delDate'を設定する -->
                <td><input type='button' value='削除' onClick='delData()'></td>
            </tr>
        </table>
        </form>
        </div>

        <?php
            }else{
                // エラーフラグが立っている場合TOPページヘのリンクを表示
                echo 'エラーが発生しました。<br/>
                      <a href="./index.php">トップページに戻る</a>';
            }
        ?>
    </body>
</html>
