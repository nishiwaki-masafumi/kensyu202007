<!DOCTYPE html>
<!-- 社員情報検索(#01) -->

<!-- CSS呼び出し -->
<link rel="stylesheet" type="text/css" href="./include/style.css">

<!-- FROM部品の配列内容をチェック
<pre>
<?php
    // var_dump($_GET);
?>
</pre>
-->

<?php
    // common
    // incluede('./include/functions.php');
    $DB_DSN = 'mysql:host=localhost; dbname=mnishiwaki; charset=utf8';
    $DB_USER = 'webaccess';
    $DB_PW = 'toMeu4rH';
    $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
    // $pdo = initDB();

    // WHEREでSQL検索
    $query_str = "SELECT member_ID,name,seibetu,section_name,grade_name
                  FROM member AS mb
                  LEFT JOIN section1_master AS sm
                  ON mb.section_ID = sm.ID
                  LEFT JOIN grade_master AS gm
                  ON mb.grade_ID = gm.ID
                  WHERE 1=1";

    $param_name = '';
    if(isset($_GET['name']) && $_GET['name'] != ""){
        $param_name = $_GET['name'];
        $query_str .= " AND mb.name LIKE '%" . $param_name . "%'";
    }

    $param_seibetu = '';
    if(isset($_GET['seibetu']) && $_GET['seibetu'] != ""){
        $param_seibetu = $_GET['seibetu'];
        $query_str .= " AND mb.seibetu = '" . $param_seibetu . "'";
    }

    $param_section = '';
    if(isset($_GET['section']) && $_GET['section'] != ""){
        $param_section = $_GET['section'];
        $query_str .= " AND sm.ID  = '" . $param_section . "'";
    }

    $param_grade = '';
    if(isset($_GET['grade']) && $_GET['grade'] != ""){
        $param_grade = $_GET['grade'];
        $query_str .= " AND gm.ID  = '" . $param_grade . "'";
    }

    // SQLの検索結果を$resultに格納
    // echo $query_str . '<br/><br/>';
    $sql = $pdo->prepare($query_str);
    $sql->execute();
    $result = $sql->fetchAll();

?>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width,initial-scale=1'>
        <link rel='stylesheet' href='./include/style.css'>
        <title>社員名簿システム</title>
    </head>

    <script "text/javascript">
        function formReset(){
            document.kensaku.name.value = "";
            document.kensaku.seibetu.value = "";
            document.kensaku.section.value = "";
            document.kensaku.grade.value = "";
        }
    </script>

    <body>

        <?php
            // ヘッダー部取り込み
            include("./include/header.php");
            // 性別、出身地コード置き換え部取り込み
            include('./include/statics.php');
            // 部署、役職IDのDB検索処理取り込み
            include('./include/function.php');
         ?>

        <!-- formタグにJS処理をするためのname「kensaku」を定義 -->
        <form method='GET' name="kensaku" action='./index.php'>
            <table align='center'>
                <tr>
                    <th>名前：</th>
                    <td colspan='5'><input type='text' size='40' maxlength="30" name='name'value="<?php echo $param_name ?>"></td>
                </tr>
                <tr>
                    <th>性別：</th>
                    <td>
                        <select name='seibetu'>
                            <!-- すべてを選択する場合、value空でWHERE文のAND条件を外す -->
                            <option value=''>すべて</option>
                            <option value='1' <?php if($param_seibetu == '1'){echo "selected";}?> >男</option>
                            <option value='2' <?php if($param_seibetu == '2'){echo "selected";}?> >女</option>
                        </select>
                    </td>
                    <th>部署：</th>
                    <td>
                        <select name='section'>
                            <option value=''>すべて</option>

                            <?php
                                $result_section = getSection();
                                foreach($result_section as $each){
                                    if($param_section == $each['ID']){
                                        echo "<option value= '" . $each['ID'] . "' selected>" . $each['section_name'] . "</option>";
                                    }else{
                                        echo "<option value= '" . $each['ID'] . "'>" . $each['section_name'] . "</option>";
                                    }
                                }
                            ?>

                            <!-- 以前の処理。事業部をハードコードしている
                            <option value='1' <?php if($param_section == '1'){echo "selected";}?> >第一事業部</option>
                            <option value='2' <?php if($param_section == '2'){echo "selected";}?> >第二事業部</option>
                            <option value='3' <?php if($param_section == '3'){echo "selected";}?> >営業</option>
                            <option value='4' <?php if($param_section == '4'){echo "selected";}?> >総務</option>
                            <option value='5' <?php if($param_section == '5'){echo "selected";}?> >人事</option>
                            -->

                        </select>
                    </td>
                    <th>役職：</th>
                    <td>
                        <select name='grade'>
                            <option value=''>すべて</option>

                            <?php
                                $result_grade = getGrade();
                                foreach($result_grade as $each){
                                    if($param_section == $each['ID']){
                                        echo "<option value= '" . $each['ID'] . "' selected>" . $each['grade_name'] . "</option>";
                                    }else{
                                        echo "<option value= '" . $each['ID'] . "'>" . $each['grade_name'] . "</option>";
                                    }
                                }
                            ?>


                            <!-- 以前の処理。役職をハードコードしている
                            <option value='1' <?php if($param_grade == '1'){echo "selected";}?> >事業部長</option>
                            <option value='2' <?php if($param_grade == '2'){echo "selected";}?> >部長</option>
                            <option value='3' <?php if($param_grade == '3'){echo "selected";}?> >チームリーダー</option>
                            <option value='4' <?php if($param_grade == '4'){echo "selected";}?> >リーダー</option>
                            <option value='5' <?php if($param_grade == '5'){echo "selected";}?> >メンバー</option>
                            -->
                        </select>
                    </td>
                </tr>
            </table>
            <table align="center">
                <tr>
                    <td>
                        <input type='submit' value='検索'>
                    </td>
                    <!-- JSの処理を呼び出すためにonClick='formReset'を設定する -->
                    <td>
                        <input type='button' value='リセット' onClick='formReset()'>
                    </td>
                </tr>
            </table>
        </form>

        <hr>

        <!--
        <pre>
        <?php
            //var_dump($result_grade);
        ?>
        </pre>
        <pre>
        <?php
             //var_dump($each);
        ?>
        </pre>
        -->


        <div class='serch-result'>
        検索結果：<?php echo count($result);?>件
        <br/>
        <table table border='1' class='serch-result-table'>
        <tr>
            <th width='100' class='th-color'>社員ID</th>
            <th width='200' class='th-color'>名前</th>
            <th width='200' class='th-color'>性別</th>
            <th width='200' class='th-color'>部署</th>
            <th width='200' class='th-color'>役職</th>
        </tr>

        <?php
        // count($result)で取得した検索結果（配列数の値）がゼロか比較する
        //ゼロでない場合はSQLの検索結果を表示、ゼロの場合は検索結果なしを表示
        if(count($result) != 0){
            foreach($result as $each){
                // var_dumpで配列の添字を見ると結合したテーブル名が含まれていない点を考慮する
                echo "<tr><td align = right>" . $each['member_ID'] . "</td>"
                       . "<td><a href='./detail01.php?member_ID=" . $each['member_ID'] . "'>" . $each['name'] . "</a></td>"
                       . "<td>" . $gender_array[$each['seibetu']] . "</td>"
                       . "<td>" . $each['section_name'] . "</td>"
                       . "<td>" . $each['grade_name'] . "</td></tr>";
            }
        }else{
            echo "<tr><td align='center' colspan='5'>検索結果なし</td></tr>";
        }
        ?>
        </table>
        <br/>
        </div>
    </body>
</html>
