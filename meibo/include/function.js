// 入力チェック
function check_Data(){
    var temp_name = document.entry.name.value;
    temp_name = temp_name.replace(/\s+/g,"");
    document.entry.name.value = temp_name;
    if(temp_name == ""){
        alert("名前を入力してください。");
        return false;
    }

    var temp_from = document.entry.pref.value;
    if(temp_from == ""){
        alert("都道府県を選択してください。");
        return false;
    }

    var temp_age = document.entry.age.value;
    if(temp_age == ""){
        alert("年齢を入力してください。");
        return false;
    }else if(temp_age < 1 || temp_age > 99){
        alert("1～99歳の間で入力してください。");
        return false;
    }

    if(window.confirm('登録を行います。よろしいですか？')){
        document.entry.submit();
    }
}
