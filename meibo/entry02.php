<!DOCTYPE html>
<!-- 新規社員情報登録(#02)リダイレクト用ページ -->

<?php
    $DB_DSN = 'mysql:host=localhost; dbname=mnishiwaki; charset=utf8';
    $DB_USER = 'webaccess';
    $DB_PW = 'toMeu4rH';
    $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
    // $pdo = initDB();

    // $_POSTで取れた名前情報を$param_nameに格納する
    $param_name = '';
    if(isset($_POST['name']) && $_POST['name'] != ""){
        $param_name = $_POST['name'];
    }

    // $_POSTで取れた名前情報を$param_prefに格納する
    $param_pref = '';
    if(isset($_POST['pref']) && $_POST['pref'] != ""){
        $param_pref = $_POST['pref'];
    }

    // $_POSTで取れた名前情報を$param_seibetuに格納する
    $param_seibetu = '';
    if(isset($_POST['seibetu']) && $_POST['seibetu'] != ""){
        $param_seibetu = $_POST['seibetu'];
    }

    // $_POSTで取れた名前情報を$param_ageに格納する
    $param_age = '';
    if(isset($_POST['age']) && $_POST['age'] != ""){
        $param_age = $_POST['age'];
    }

    // $_POSTで取れた名前情報を$param_sectionに格納する
    $param_section = '';
    if(isset($_POST['section']) && $_POST['section'] != ""){
        $param_section = $_POST['section'];
    }

    // $_POSTで取れた名前情報を$param_gradeに格納する
    $param_grade = '';
    if(isset($_POST['grade']) && $_POST['grade'] != ""){
        $param_grade = $_POST['grade'];
    }

    // SQL検索処理
    $query_str = "INSERT INTO member (member_ID,name,pref,seibetu,age,section_ID,grade_ID)
                  VALUES (NULL,'".$param_name."','".$param_pref."','"
                                 .$param_seibetu."','".$param_age."','"
                                 .$param_section."','".$param_grade."')";

                // 生SQL例
                // INSERT INTO `member` (`member_ID`, `name`, `pref`, `seibetu`, `age`, `section_ID`, `grade_ID`)
                // VALUES (NULL, '田端士郎', '18', '1', '25', '1', '5');

    // SQL出力結果を$resultに格納
    echo $query_str . '<br/><br/>';
    $sql = $pdo->prepare($query_str);
    $sql->execute();
    // $result = $sql->fetchAll();

    // 最後に登録されたmember_IDを取得
    $id = $pdo->lastInsertId('member_ID');

    // detail01.phpへのリダイレクト処理
    $url = "./detail01.php?member_ID=" . $id;
    header('Location: ' . $url);
    exit;
?>

<html>
    <head>
    </head>
    <body>
    </body>
</html>
