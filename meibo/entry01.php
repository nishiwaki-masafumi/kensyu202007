<!DOCTYPE html>
<!-- 新規社員情報登録(#02) -->

<?php
    // 性別、出身地コード置き換え部取り込み
    include("./include/statics.php");
    // 部署、役職IDのDB検索処理取り込み
    include('./include/function.php');
?>

<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width,initial-scale=1'>
        <link rel='stylesheet' href='./include/style.css'>
        <title>社員名簿システム</title>
    </head>

    <!-- JS処理 -->
    <!-- function.js取り込み -->
    <script src="./include/function.js"></script>

    <body>
        <!-- ヘッダーHTMLの取り込み -->
        <?php include("./include/header.php"); ?>
        <br/>
        <div class='detail-result'>
        <!-- form部品で入力した内容をPOSTでentry02.phpに渡してcreate処理する -->
        <!-- formタグにJS処理をするためのname「sakusei」を定義 -->
        <form method='POST' name='entry' action='./entry02.php'>
            <table table border='1'  width='100%' class='detail-result-table'>
            <tr>
                <th>名前</th>
                <td><input type='text' size='40' maxlength="30" name='name'></td>
            </tr>
            <tr>
                <th>出身地</th>
                <td>
                    <select name='pref'>
                        <option value=''>都道府県</option>
                        <?php
                            // $pref_arrayをforeachで回して、keyとvalueを取得
                            foreach($pref_array as $pref_number => $pref_name){
                                // HTMLでoption valueの書き方を想像する
                                echo "<option value='" . $pref_number . "'>" . $pref_name . "</option>";
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <th>性別</th>
                <td>
                    <input type='radio' name='seibetu' value='1' checked>男
                    <input type='radio' name='seibetu' value='2'>女
                </td>
            </tr>
            <tr>
                <th>年齢</th>
                <td><input type='number' name='age' min="1" max="99">才</td>
            </tr>
            <tr>
                <th>所属部署</th>
                <td>
                    <?php
                        //getSection()で部署IDと部署名のDB検索結果を取得する
                        $result_section = getSection();
                        foreach($result_section as $each){
                            // 部署IDが1の場合はラジオボタンの属性に"checked"を付与する
                            if($each['ID'] == "1"){
                                echo "<input type='radio' name='section'
                                       value='". $each['ID'] . "' checked>" . $each['section_name'];
                            }else{
                                echo "<input type='radio' name='section'
                                       value='". $each['ID'] . "'>" . $each['section_name'];
                            }
                        }
                    ?>

                    <!-- 以前の処理 部署名をハードコードしている
                    <input type='radio' name='section' value='1' checked>第一事業部
                    <input type='radio' name='section' value='2'>第二事業部
                    <input type='radio' name='section' value='3'>営業
                    <input type='radio' name='section' value='4'>総務
                    <input type='radio' name='section' value='5'>人事
                    -->
                </td>
            </tr>
            <tr>
                <th>役職</th>
                <td>
                    <?php
                        //getGrade()で役職IDと役職名のDB検索結果を取得する
                        $result_grade = getGrade();
                        foreach($result_grade as $each){
                            // 役職IDが1の場合はラジオボタンの属性に"checked"を付与する
                            if($each['ID'] == "1"){
                                echo "<input type='radio' name='grade'
                                       value='". $each['ID'] . "' checked>" . $each['grade_name'];
                            }else{
                                echo "<input type='radio' name='grade'
                                       value='". $each['ID'] . "'>" . $each['grade_name'];
                            }
                        }
                    ?>

                    <!-- 以前の処理 役職名をハードコードしている
                    <input type='radio' name='grade' value='1' checked>事業部長
                    <input type='radio' name='grade' value='2'>部長
                    <input type='radio' name='grade' value='3'>チームリーダー
                    <input type='radio' name='grade' value='4'>リーダー
                    <input type='radio' name='grade' value='5'>メンバー
                    -->
                </td>
            </tr>

            </table>
            <br/>
            <!-- JSの処理を呼び出すためにonClick='check_Data'を設定する -->
            <input type='button' value='登録' onClick='check_Data()'>
            <input type='reset' value='リセット'>
        </form>
        <br/>
        </div>
    </body>
</html>
