<!DOCTYPE html>
<!-- 既存社員情報修正(#04) -->


<!-- <pre>
<?php
    // $_GETの中身（URLのGETパラメータ）を出力する
    // var_dump($_GET);
?>
</pre> -->

<?php
    // common
    // 性別、出身地コード置き換え部取り込み
    include('./include/statics.php');
    // 部署、役職IDのDB検索処理取り込み
    include('./include/function.php');

    $DB_DSN = 'mysql:host=localhost; dbname=mnishiwaki; charset=utf8';
    $DB_USER = 'webaccess';
    $DB_PW = 'toMeu4rH';
    $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
    // $pdo = initDB();

    // $param_IDを定義
    // $_GETが存在するかつ中身が空でない場合、$_GETの中身を$param_IDに格納
    $param_ID = "";
    $error = "0";
    if(isset($_POST['member_ID']) && $_POST['member_ID'] != ""){
        $param_ID = $_POST['member_ID'];
    }else{
        $error = "1";  // エラーフラグ設定
    }
?>

<?php
    // URLの$_GETパラメータが$paramに格納されているか確認
    // echo "param_ID = " . $param_ID . "<br/><br/>";

    // WHERE条件に指定しているメンバーIDのパラメータは$_GETの値を$param_IDに格納したもの
    // 今回はsection_IDとgrade_IDが欲しいのでテーブル結合はしない
    $query_str = "SELECT mb.member_ID,mb.name,mb.pref,mb.seibetu,mb.age,mb.section_ID,mb.grade_ID
                  FROM member AS mb
                  /* LEFT JOIN section1_master AS sm
                  ON mb.section_ID = sm.ID
                  LEFT JOIN grade_master AS gm
                  ON mb.grade_ID = gm.ID */
                  WHERE member_ID = " . $param_ID;

    // SQLの検索結果を$resultに格納
    // echo $query_str . '<br/><br/>';
    $sql = $pdo->prepare($query_str);
    $sql->execute();
    $result = $sql->fetchAll();

    $result_cnt = count($result);
    if($result_cnt != "1"){
        $error = "1";  // エラーフラグ設定
    }
?>

<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width,initial-scale=1'>
        <link rel='stylesheet' href='./include/style.css'>
        <title>社員名簿システム</title>
    </head>

    <!-- JS処理  function.js取り込み -->
    <script src="./include/function.js"></script>

    <!-- function.js取り込み前の処理
    <script type="text/javascript">
        function upData(){
            if(window.confirm('更新を行います。よろしいですか？')){
                //documentはHTML全体。koushinフォームをsubmitする
                document.koushin.submit();
            }
        }
    </script>
    -->

    <body>
        <!-- ヘッダーHTMLの取り込み -->
        <?php include("./include/header.php"); ?>

        <!--<pre>
        <?php
             // var_dumpで二次元連想配列の内容を確認
             // var_dump($result);
        ?>
        -->
        </pre>
        <div class='detail-result'>
        <br/>
        <!-- formタグにJS処理をするためのname「koushin」を定義 -->
        <form method='POST' name='entry' action='./entry_update02.php'>
        <!--<form method='POST' name='koushin' action='./entry_update02.php'>-->
        <table table border='1' width='100%' class='detail-result-table'>
        <?php
            // エラーフラグ判定
            if($error == "0" ){
                $each = $result[0];
        ?>

            <tr>
                <th>社員ID</td>
                <td><?php echo $each['member_ID'] ?></td>
            </tr>
            <tr>
                <th>名前</td>
                <td><input type='text' size='40' maxlength="30" name='name' value='<?php echo $each['name'] ?>'></td>
            </tr>
            <tr>
                <th>出身地</td>
                <td>
                    <select name='pref'>
                        <?php
                            // $pref_arrayをforeachで回して、keyとvalueを取得
                            foreach($pref_array as $pref_number => $pref_name){
                                // HTMLでoption valueの書き方を想像する
                                if($pref_number == $each['pref']){
                                    echo "<option value='" . $pref_number . "' selected>" . $pref_name . "</option>";
                                }else{
                                    echo "<option value='" . $pref_number . "'>" . $pref_name . "</option>";
                                }
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <th>性別</td>
                <td>
                    <input type='radio' name='seibetu' value='1'
                    <?php if($each['seibetu'] == '1'){ echo 'checked'; }?>>男
                    <input type='radio' name='seibetu' value='2'
                    <?php if($each['seibetu'] == '2'){ echo 'checked'; }?>>女
                </td>
            </tr>
            <tr>
                <th>年齢</td>
                <td><input type='number' name='age' min="1" max="99" value='<?php echo $each['age'] ?>'>才</td>
            </tr>
            <tr>
                <th>所属部署</td>
                <td>
                    <?php
                        //getSection()で部署IDと部署名のDB検索結果を取得する
                        $result_section = getSection();
                        foreach($result_section as $each2){
                            // $result_sectionと$resultの部署IDを比較して一致した場合
                            //ラジオボタンの属性に"checked"を付与する
                            if($each2['ID'] == $each['section_ID']){
                                echo "<input type='radio' name='section'
                                       value='". $each2['ID'] . "' checked>" . $each2['section_name'];
                            }else{
                                echo "<input type='radio' name='section'
                                       value='". $each2['ID'] . "'>" . $each2['section_name'];
                            }
                        }
                    ?>

                    <!-- 以前の処理 部署名をハードコードしている
                    <input type='radio' name='section' value='1'
                    <?php //if($each['section_ID'] == '1'){ echo 'checked'; }?>>第一事業部
                    <input type='radio' name='section' value='2'
                    <?php //if($each['section_ID'] == '2'){ echo 'checked'; }?>>第二事業部
                    <input type='radio' name='section' value='3'
                    <?php //if($each['section_ID'] == '3'){ echo 'checked'; }?>>営業
                    <input type='radio' name='section' value='4'
                    <?php //if($each['section_ID'] == '4'){ echo 'checked'; }?>>総務
                    <input type='radio' name='section' value='5'
                    <?php //if($each['section_ID'] == '5'){ echo 'checked'; }?>>人事
                    -->
                </td>
            </tr>
            <tr>
                <th>役職</td>
                <td>
                    <?php
                        //getGrade()で部署IDと部署名のDB検索結果を取得する
                        $result_grade = getGrade();
                        foreach($result_grade as $each2){
                            // $result_gradeと$resultの部署IDを比較して一致した場合
                            //ラジオボタンの属性に"checked"を付与する
                            if($each2['ID'] == $each['grade_ID']){
                                echo "<input type='radio' name='grade'
                                       value='". $each2['ID'] . "' checked>" . $each2['grade_name'];
                            }else{
                                echo "<input type='radio' name='grade'
                                       value='". $each2['ID'] . "'>" . $each2['grade_name'];
                            }
                        }
                    ?>

                    <!-- 以前の処理 役職名をハードコードしている
                    <input type='radio' name='grade' value='1'
                    <?php //if($each['grade_ID'] == '1'){ echo 'checked'; }?>>事業部長
                    <input type='radio' name='grade' value='2'
                    <?php //if($each['grade_ID'] == '2'){ echo 'checked'; }?>>部長
                    <input type='radio' name='grade' value='3'
                    <?php //if($each['grade_ID'] == '3'){ echo 'checked'; }?>>チームリーダー
                    <input type='radio' name='grade' value='4'
                    <?php //if($each['grade_ID'] == '4'){ echo 'checked'; }?>>リーダー
                    <input type='radio' name='grade' value='5'
                    <?php //if($each['grade_ID'] == '5'){ echo 'checked'; }?>>メンバー
                    -->
                </td>
            </tr>
        </table>
        <br/>
            <!-- 登録ボタン選択で社員情報更新(リダイレクトページ)にmember_IDを渡すための処理 -->
            <input type='hidden' name='member_ID' value='<?php echo $each['member_ID'] ?>'>
            <!-- JSの処理を呼び出すためにonClick='upDate'を設定する -->
            <!--<input type='button' value='登録' onClick='upData()'>-->
            <input type='button' value='登録' onClick='check_Data()'>
            <input type='reset' value='リセット'>
        </form>
        </div>

        <?php
            }else{
                // エラーフラグが立っている場合TOPページヘのリンクを表示
                echo 'エラーが発生しました。<br/>
                      <a href="./index.php">トップページに戻る</a>';
            }
        ?>

        <!--
        <pre>
        <?php
            // var_dumpで二次元連想配列の内容を確認
            // var_dump($result_section);
        ?>
        </pre>
        -->


    </body>
</html>
