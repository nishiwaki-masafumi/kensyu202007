<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>第四回課題 配列の学習1 西脇</title>
  </head>
  <body>
    <h1>第四回課題 配列の基本的な書き方1 西脇</h1>
    <h2>配列の基本的な書きかた・表示のさせ方</h2>
    <?php
        $fruit = array('りんご','すいか','みかん','なし','イチゴ','かき');
        echo $fruit[3] . '<br/>'; //なし と表示される
        echo $fruit[0] . '<br/>'; //りんご と表示される
        echo $fruit[9] . '<br/>'; //エラーが表示される

        $fruit[2] = 'いちじく'; //[2]が「みかん」→「いちじく」に上書きされる
        $fruit[6] = 'キウイ'; //[6]にキウイが追加される
    ?>
    <h2>var_dumpで配列の内容を出力</h2>
    <pre>
    <?php var_dump($fruit); ?>
    </pre>
  </body>
</html>
