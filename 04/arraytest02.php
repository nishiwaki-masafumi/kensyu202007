<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>第四回課題 配列の学習2 西脇</title>
  </head>
  <body>
    <h1>第四回課題 配列の学習2 西脇</h1>
    <?php
        //配列の定義
        //書き方その1
        $fruit1 = array('りんご1','すいか1','みかん1','なし1','イチゴ1','かき1');

        //書き方その2
        $fruit2 = ['りんご2','すいか2','みかん2','なし2','イチゴ2','かき2'];

        //書き方その3
        $fruit3[0] = 'りんご3';
        $fruit3[1] = 'すいか3';
        $fruit3[2] = 'みかん3';
        $fruit3[3] = 'なし3';
        $fruit3[4] = 'イチゴ3';
        $fruit3[5] = 'かき3';
    ?>

    <h2>for()文の利用</h2>
    <?php
        for($i=0; $i < count($fruit1); $i++){
            echo $fruit1[$i] . '<br/>';
        }
    ?>
    <br/>

    <h2>foreach()文の利用</h2>
    <?php
        foreach($fruit2 as $each){
            echo $each . '<br/>';
        }
    ?>

    <h2>配列の内容を検索</h2>
    <?php
        //配列の内容を検索する文字列を変数$needleに定義
        $needle = 'みかん3';

        //in_arrayで$fruit3にいる「みかん3」を検索する
        if(in_array($needle, $fruit3)){
            echo $needle . 'がfruit3の要素の値に存在しています';
        }else{
            echo $needle . 'がfruit3の要素の値に存在していません';
        }
    ?>
    <br/>
    <h2>var_dumpで配列の内容を出力</h2>
    <pre>
    <?php
        var_dump($fruit1);
        var_dump($fruit2);
        var_dump($fruit3);
    ?>
    </pre>
  </body>
</html>
